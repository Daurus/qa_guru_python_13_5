import time

from selene import browser, have
from pathlib import Path


def test_demoqa_setting_form_fields():

    browser.open('/automation-practice-form')

    browser.element('#firstName').type('Тестер')
    browser.element('#lastName').type('Тестович')
    browser.element('#userEmail').type('dautovseo@yandex.ru')
    browser.element('[for="gender-radio-1"]').click()
    browser.element('#userNumber').type('7921412412')

    browser.element('#dateOfBirthInput').click()
    browser.element('.react-datepicker__month-select').click().element('[value="7"]').click()
    browser.element('.react-datepicker__year-select').click().element('[value="1996"]').click()
    browser.all('.react-datepicker__day--003').first.click()

    browser.element('#subjectsInput').type('Hindi')
    browser.element('#react-select-2-option-0').click()
    browser.element('[for="hobbies-checkbox-2"]').click()
    browser.element('#uploadPicture').set_value((str(Path(__file__).parent.joinpath(f'img/test_img.jpg'))))
    browser.element('#currentAddress').type('RandomText555111#@#!%^')

    browser.element('#state').click()
    browser.element('#react-select-3-option-3').click()

    browser.element('#city').click()
    browser.element('#react-select-4-option-1').click()

    browser.element('#submit').click()

    browser.element(".modal-content").element("tbody").all("tr").all("td").even.should(have.exact_texts((
        'Тестер Тестович',
        'dautovseo@yandex.ru',
        'Male',
        '7921412412',
        '03 August,1996',
        'Hindi',
        'Reading',
        'test_img.jpg',
        'RandomText555111#@#!%^',
        'Rajasthan Jaiselmer'
    )))

